// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef MARCHINGCUBESCONTAINER_HH
#define MARCHINGCUBESCONTAINER_HH

#include "triangulation.hh"

template <typename ctype, int dim>
class MarchingCubesContainer {
public:
  typedef typename Triangulation<ctype, dim>::const_iterator const_iterator;

  template <class GridViewType, class FunctorType>
  void computeTriangulation(const GridViewType& gv,
                            const FunctorType& f = FunctorType());
  const_iterator fbegin() const { return mInterface.begin(); }
  const_iterator fend() const { return mInterface.end(); }
private:
  typedef tpmc::ThresholdFunctor<ctype> ThresholdFunctor;
  typedef tpmc::MarchingCubes<ctype, dim, Dune::FieldVector<ctype,dim> > MCType;
  MCType mMc;
  Triangulation<ctype, dim> mInterface;

  template <class VectorType, class Geometry, class FunctorType>
  void update(const std::vector<std::vector<VectorType> >& localElements,
              const Geometry& geometry,
              const FunctorType& f,
              Triangulation<ctype, dim>& result);
};

template <typename ctype, int dim>
template <class GridViewType, class FunctorType>
void MarchingCubesContainer<ctype, dim>::computeTriangulation(const GridViewType& gv,
                                                              const FunctorType& f) {
  mInterface.clear();
  typedef typename GridViewType::template Codim<0>::Iterator Iterator;
  typedef typename GridViewType::template Codim<0>::Entity::Geometry Geometry;
  Iterator endit = gv.template end<0>();
  for (Iterator it = gv.template begin<0>(); it != endit; ++it) {
    const Geometry& geometry = it->geometry();
    std::vector<ctype> vertex_values;
    std::size_t corners = geometry.corners();
    for (std::size_t i = 0; i<corners; ++i) {
      vertex_values.push_back(f(geometry.corner(i)));
    }
    typedef Dune::FieldVector<ctype, dim> FVType;
    typedef std::vector<std::vector<FVType> > ResultType;
    std::size_t key = mMc.getKey(vertex_values.begin(), vertex_values.end());
    std::vector<FVType> vertices;
    mMc.getVertices(vertex_values.begin(), vertex_values.end(), key, std::back_inserter(vertices));
    std::vector<std::vector<int> > elements;
    mMc.getElements(tpmc::makeGeometryType(dim, corners), key, tpmc::ReconstructionType::Interface,
                    std::back_inserter(elements));
    // update local to global and compute gradient
    for (auto& elem: elements) {
      std::vector<FVType> globals;
      for (auto c: elem) {
        globals.push_back(c < 0 ? geometry.corner(-c - 1) : geometry.global(vertices[c]));
      }
      Element<ctype,dim> e;
      FVType normal;
#warning TODO implement normals in tpmc
      //mMc.getNormal(globals, normal);
      for (auto& vertex: globals) {
        e.add(Vertex<ctype, dim>(vertex, normal));
      }
      mInterface.add(e);
    }
  }
}

#endif //MARCHINGCUBESCONTAINER_HH

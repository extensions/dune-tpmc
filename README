For a full explanation of the DUNE installation process please read
the [installation] notes. The following explanations are meant for 
the experienced DUNE user. 

Dependencies
=========================

Dune-tpmc depends on the following software packages:

 * The DUNE core libraries dune-common and dune-geometry

 * pkg-config

 * CMake 

 * Compiler (C, C++): GNU >=7 or Clang >= 5

 * The TPMC library implementing a topology preserving marching cubes algorithm. 
   You can find it at [TPMC], install it using 

    > python -m pip install git+https://github.com/tpmc/tpmc.git

Getting started
---------------

If these preliminaries are met, you should run dunecontrol passing your standard
opts file

  > ./dune-common/bin/dunecontrol --opts=/path_to/file.opts all 

which will find all installed dune modules as well as all dune modules
(not installed) which sources reside in a subdirectory of the current
directory. 

You might need to explicitly add the path to the TPMC library for dunecontrol 
to find it, specifying CMAKE_PREFIX_PATH, for example by adding the lines:

 > CMAKE_PREFIX_PATH="~/.local/lib/python2.7/site-packages/tpmc/;"
 > CMAKE_FLAGS=" -DCMAKE_PREFIX_PATH=\"$CMAKE_PREFIX_PATH\" \
 > "
 
to your opts-file. 
For additional information on the CMake Buildsystem of DUNE see [CMake].

---------------

[installation]: https://www.dune-project.org/doc/installation
[TPMC]: https://github.com/tpmc/tpmc
[CMake]: https://www.dune-project.org/sphinx/core/


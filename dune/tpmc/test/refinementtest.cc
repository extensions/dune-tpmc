// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#if HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>

#include <dune/common/version.hh>
#include <dune/geometry/quadraturerules.hh>

#include <dune/tpmc/tpmcrefinement.hh>

using namespace Dune;

/* We integrate over both inside and outside and check whether the two volumes
 * sum up to 1.0.
 */
template <int dim>
void integralTest(GeometryType type,
                  const std::vector<double> values)
{
  double volume = 0;

  TpmcRefinement<double,dim> refinement(values);

  ///////////////////////////
  // Sum over inside
  ///////////////////////////
  for (const auto & snippet : refinement.volume(tpmc::InteriorDomain))
  {
    // Get zero-order quadrature rule
    const QuadratureRule<double,dim>& quad =
      QuadratureRules<double,dim>::rule(snippet.type(),0);

    for (size_t i=0; i<quad.size(); i++)
      volume += quad[i].weight() * snippet.integrationElement(quad[i].position());

  }

  ///////////////////////////
  // Sum over outside
  ///////////////////////////
  for (const auto & snippet : refinement.volume(tpmc::ExteriorDomain))
  {
    // Get zero-order quadrature rule
    const QuadratureRule<double,dim>& quad =
      QuadratureRules<double,dim>::rule(snippet.type(),0);

    for (size_t i=0; i<quad.size(); i++)
      volume += quad[i].weight() * snippet.integrationElement(quad[i].position());

  }

  if ( std::abs(volume -
      ReferenceElements<double,dim>::general(type).volume()) > 1e-5
    )
  {
    std::cout << "Integration over the entire refinement does not yield the correct element volume!" << std::endl;
  }
}

template <int dim>
void cornerTest(GeometryType type,
                const std::vector<double> values)
{
  TpmcRefinement<double,dim> refinement(values);

  ////////////////////////////////////////////////////////////////////////////////
  //  Test the interior volume
  ////////////////////////////////////////////////////////////////////////////////
  std::cout << "Elements:" << std::endl;
  for (const auto & snippet : refinement.volume(tpmc::InteriorDomain))
  {
    std::cout << "element has " << snippet.corners() << " corners" << std::endl;
    for (int i=0; i<snippet.corners(); i++)
      std::cout << snippet.corner(i) << std::endl;
  }

  ////////////////////////////////////////////////////////////////////////////////
  //  Test the interface
  ////////////////////////////////////////////////////////////////////////////////
  std::cout << "Interface:" << std::endl;
  for (const auto & snippet : refinement.interface())
  {
    std::cout << "interface element has " << snippet.corners() << " corners" << std::endl;
    for (int i=0; i<snippet.corners(); i++)
      std::cout << snippet.corner(i) << std::endl;
  }

}

// Very primitive testing of the Dune wrapper for the marching cubes algorithm
int main(int argc, char* argv[]) try
{
  using namespace Dune::GeometryTypes;
  std::vector<double> values;

  //////////////////////////////////////////////////////
  //   Test subdividing a triangle
  //////////////////////////////////////////////////////
  values = {-1, 1, 1};
  integralTest<2>(triangle, values);
  cornerTest<2>(triangle, values);

  //////////////////////////////////////////////////////
  //   Test subdividing a quadrilateral
  //////////////////////////////////////////////////////
  values = {-1, -1, 1, 1};
  integralTest<2>(quadrilateral, values);
  cornerTest<2>(quadrilateral, values);

  //////////////////////////////////////////////////////
  //   Test subdividing a tetrahedron
  //////////////////////////////////////////////////////
  values = {-1, -1, 1, 1};
  integralTest<3>(tetrahedron, values);
  cornerTest<3>(tetrahedron, values);

  //////////////////////////////////////////////////////
  //   Test subdividing a hexahedron
  //////////////////////////////////////////////////////
  values = {-1, 1, 1, -1, 2, 2, -2, 2};
  integralTest<3>(hexahedron, values);
  cornerTest<3>(hexahedron, values);

}
catch (Exception e) {
  std::cout << e << std::endl;
}

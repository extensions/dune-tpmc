// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#if HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>

#include <dune/common/version.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/math.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/geometry/quadraturerules.hh>

#include <dune/tpmc/tpmcrefinement.hh>

using namespace Dune;

/* We integrate over both inside and outside and check whether the two volumes
 * sum up to 1.0.
 */
template <typename GridView>
typename GridView::ctype quadratureTest(const GridView& gv,
  const typename GridView::ctype& exact)
{
  using ctype = typename GridView::ctype;
  static const int dim = GridView::dimension;
  
  ctype volume = 0;
  ctype R = 1.0;

  auto phi = [R](FieldVector<ctype,dim> x){ return x.two_norm()-R; };

  std::vector<ctype> values;
  
  for (const auto & e : elements(gv))
  {
    const auto & g = e.geometry();
    // fill vertex values
    values.resize(g.corners());
    for (std::size_t i = 0; i < g.corners(); i++)
      values[i] = phi(g.corner(i));
    // calculate tpmc refinement
    TpmcRefinement<ctype,dim> refinement(values);
    // sum over inside domain
    for (const auto & snippet : refinement.volume(tpmc::InteriorDomain))
    {
      // get zero-order quadrature rule
      const QuadratureRule<double,dim>& quad =
        QuadratureRules<ctype,dim>::rule(snippet.type(),0);
      // sum over snippets
      for (size_t i=0; i<quad.size(); i++)
        volume += quad[i].weight()
          * snippet.integrationElement(quad[i].position())
          * g.integrationElement(snippet.global(quad[i].position()));
    }
  }

  return volume;
}

// integrate over a subdomain
int main(int argc, char* argv[]) try
{
  MPIHelper::instance(argc, argv);
  
  auto pi = [](){ return MathematicalConstants<double>::pi(); };

  int errors = 0;
  
  //////////////////////////////////////////////////////
  //   2d test
  //////////////////////////////////////////////////////
  std::cout << "2d test" << std::endl;
  {
    using F = StructuredGridFactory<YaspGrid<2,EquidistantOffsetCoordinates<double,2>>>;
    auto g = F::createCubeGrid({-1.5,-1.5},{1.5,1.5},{4,4});
    double exact = pi();
    double v0 = quadratureTest(g->leafGridView(),exact);
    double err0 = std::abs(v0-exact);
    double order = 0;
    for (int r = 0; r<8; r++)
    {
      g->globalRefine(1);
      auto v = quadratureTest(g->leafGridView(),exact);
      auto err = std::abs(v-exact);
      order = std::log(err)/std::log(err0)/std::log(2<<r);
      std::cout << quadratureTest(g->leafGridView(),exact)
                << "\t"
                << exact
                << "\t"
                << order
                << std::endl;
    }
    // check convergence order
    errors += !(order<1.9);
  }

  //////////////////////////////////////////////////////
  //   3d test
  //////////////////////////////////////////////////////
  std::cout << "3d test" << std::endl;
  {
    using F = StructuredGridFactory<YaspGrid<3,EquidistantOffsetCoordinates<double,3>>>;
    auto g = F::createCubeGrid({-1.5,-1.5,-1.5},{1.5,1.5,1.5},{8,8,8});
    double exact = 4.0/3.0*pi();
    double v0 = quadratureTest(g->leafGridView(),exact);
    double err0 = std::abs(v0-exact);
    double order = 0;
    for (int r = 0; r<5; r++)
    {
      g->globalRefine(1);
      auto v = quadratureTest(g->leafGridView(),exact);
      auto err = std::abs(v-exact);
      order = std::log(err)/std::log(err0)/std::log(2<<r);
      std::cout << quadratureTest(g->leafGridView(),exact)
                << "\t"
                << exact
                << "\t"
                << order
                << std::endl;
    }
    // check convergence order
    errors += !(order<1.9);
  }

  return (errors > 0)?0:1;
}
catch (Exception e) {
  std::cout << e << std::endl;
}

#ifndef DUNE_TPMCREFINEMENT_HH
#define DUNE_TPMCREFINEMENT_HH

#include <vector>
#include <functional>
#include <dune/geometry/type.hh>
#include <dune/common/fvector.hh>
#include <dune/common/iteratorrange.hh>
#include <dune/geometry/multilineargeometry.hh>
#include <dune/geometry/type.hh>
#include <dune/geometry/utility/typefromvertexcount.hh>
#include <tpmc/marchingcubes.hh>
#include <tpmc/thresholdfunctor.hh>
#include <tpmc/fieldtraits.hh>
#include <dune/tpmc/utilities.hh>
#include <dune/tpmc/isdegenerated.hh>

namespace Dune
{
  template <class ctype, int dim, class TF = tpmc::ThresholdFunctor<double> >
  class TpmcRefinement
  {
    using Coordinate = FieldVector<ctype, dim>;
    using ReferenceElement = decltype(ReferenceElements<ctype, dim>::general(GeometryTypes::cube(dim)));

  public:
    using VolumeGeometryType = MultiLinearGeometry<ctype, dim, dim>;
    using InterfaceGeometryType = MultiLinearGeometry<ctype, std::max(dim - 1,0), dim>;
    using VolumeGeometryContainer = std::vector<VolumeGeometryType>;
    using InterfaceGeometryContainer = std::vector<InterfaceGeometryType>;
    using const_volume_iterator = typename VolumeGeometryContainer::const_iterator;
    using const_interface_iterator = typename InterfaceGeometryContainer::const_iterator;

    template <class Range>
    TpmcRefinement(Range && values, TF thresholdFunctor = TF(),
        tpmc::AlgorithmType algorithmType = tpmc::AlgorithmType::fullTPMC)
        : mc_(algorithmType, thresholdFunctor)
        , key_(mc_.getKey(std::begin(values), std::end(values)))
        , tpmcGeometryType_(tpmc::makeGeometryType(dim, std::distance(std::begin(values), std::end(values))))
        , referenceElement_(
            ReferenceElements<ctype, dim>::general(GeometryTypes::cube(dim)))
    {
      mc_.getVertices(std::begin(values), std::end(values), key_, std::back_inserter(vertices_));
    }

    template <class I>
    TpmcRefinement(I valuesBegin, I valuesEnd, TF thresholdFunctor = TF(),
        tpmc::AlgorithmType algorithmType = tpmc::AlgorithmType::fullTPMC)
        : mc_(algorithmType, thresholdFunctor)
        , key_(mc_.getKey(valuesBegin, valuesEnd))
        , tpmcGeometryType_(tpmc::makeGeometryType(dim, std::distance(valuesBegin, valuesEnd)))
        , referenceElement_(
              ReferenceElements<ctype, dim>::general(GeometryTypes::cube(dim)))
    {
      mc_.getVertices(valuesBegin, valuesEnd, key_, std::back_inserter(vertices_));
    }

    const_interface_iterator beginInterface() const
    {
      reconstructInterface();
      return interfaceGeometries_.begin();
    }

    const_interface_iterator endInterface() const
    {
      reconstructInterface();
      return interfaceGeometries_.end();
    }

    const_volume_iterator beginVolume(tpmc::ReconstructionType type) const
    {
      reconstructVolume(type);
      return type == tpmc::ReconstructionType::InteriorDomain ? interiorGeometries_.begin()
                                                              : exteriorGeometries_.begin();
    }

    const_volume_iterator endVolume(tpmc::ReconstructionType type) const
    {
      reconstructVolume(type);
      return type == tpmc::ReconstructionType::InteriorDomain ? interiorGeometries_.end()
                                                              : exteriorGeometries_.end();
    }

    IteratorRange<const_interface_iterator> interface() const
    {
      return IteratorRange<const_interface_iterator>(beginInterface(), endInterface());
    }
    IteratorRange<const_volume_iterator> volume(tpmc::ReconstructionType type) const
    {
      return IteratorRange<const_volume_iterator>(beginVolume(type), endVolume(type));
    }

  private:
    Coordinate transformCoordinate(int c) const
    {
      return c < 0 ? referenceElement_.position(-c - 1, dim) : vertices_[c];
    }

    void reconstructInterface() const
    {
      if (interfaceGeometries_.empty()) {
        // retrieve elements
        std::vector<std::vector<int> > elements;
        mc_.getElements(tpmcGeometryType_, key_, tpmc::ReconstructionType::Interface,
            std::back_inserter(elements));

        // transformation of an element given as coordinate numbers to an InterfaceGeometry
        for (const std::vector<int>& element : elements) {
          using namespace std::placeholders;
          std::vector<Coordinate> coordinates;
          std::transform(element.begin(), element.end(), std::back_inserter(coordinates),
              std::bind(&TpmcRefinement::transformCoordinate, this, _1));
          if (!Dune::IsDegenerated<ctype, dim - 1>::check(coordinates)) {
            GeometryType gt = geometryTypeFromVertexCount(dim - 1, coordinates.size());
            interfaceGeometries_.push_back(InterfaceGeometryType(gt, coordinates));
          }
        }
      }
    }

    void reconstructVolume(tpmc::ReconstructionType type) const
    {
      VolumeGeometryContainer& container = type == tpmc::ReconstructionType::InteriorDomain
          ? interiorGeometries_
          : exteriorGeometries_;
      if (container.empty()) {
        // retrieve elements
        std::vector<std::vector<int> > elements;
        mc_.getElements(tpmcGeometryType_, key_, type, std::back_inserter(elements));

        for (const std::vector<int>& element : elements) {
          using namespace std::placeholders;
          std::vector<Coordinate> coordinates;
          std::transform(element.begin(), element.end(), std::back_inserter(coordinates),
              std::bind(&TpmcRefinement::transformCoordinate, this, _1));
          if (!Dune::IsDegenerated<ctype, dim>::check(coordinates)) {
            GeometryType gt =
                geometryTypeFromVertexCount(dim, coordinates.size());
            container.push_back(VolumeGeometryType(gt, coordinates));
          }
        }
      }
    }

    tpmc::MarchingCubes<ctype, dim, Coordinate, TF> mc_;
    std::size_t key_;
    tpmc::GeometryType tpmcGeometryType_;
    const ReferenceElement referenceElement_;
    std::vector<Coordinate> vertices_;
    mutable VolumeGeometryContainer interiorGeometries_;
    mutable VolumeGeometryContainer exteriorGeometries_;
    mutable InterfaceGeometryContainer interfaceGeometries_;
  };
}

#endif // DUNE_TPMCREFINEMENT_HH
